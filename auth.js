const jwt = require("jsonwebtoken");

const secret = "CourseBookingAPI";

// Token creation
module.exports.createAccessToken = (user) => {
	// The dataa will be received from the registration form
	// When the user logs in a token will be created with the user's information
	const data = {
		id : user._id,
		email: user.email,
		isAdmin: user.isAdmin
	};

	// Generates a json web token
	return jwt.sign(data, secret, {});
}

// Token Verification
module.exports.verify = (req, res, next)  => {
	let token = req.headers.authorization

	if(typeof token !== "undefined"){
		console.log(token);
		// Bearer
		token = token.slice(7, token.length);

		// Validate the token using the "verify" method decrytpting the token using the secret code
		return jwt.verify(token, secret, (err, data) => {

			if(err){
				return res.send({auth : "failed"});
			}
			else{
				// Allows the application to proceed with the next middleware function/callback function in the route
				next();
			}
		})
	}
	// Token does not exist
	else{
		return res.send({auth : "failed"});
	}
}

// Token decrytption
module.exports.decode = (token) => {
	if(typeof token !== "undefined"){
		token = token.slice(7, token.length);

		return jwt.verify(token, secret, (err, data) => {
			if(err){
				return null;
			}
			else{
				return jwt.decode(token, {complete:true}).payload;
			}
		})
	}
}