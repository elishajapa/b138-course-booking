const User = require("../model/User");
const Course = require("../model/Course");
const bcrypt = require("bcrypt");
const auth = require("../auth");


// Check if email already exists
module.exports.checkEmailExists = (reqBody) => {
	return User.find({email: reqBody.email}).then(result => {

		if(result.length > 0){
			return true;
		}
		else{
			return false;
		}
	})
}

// User registratiion
module.exports.registerUser = (reqBody) => {
	let newUser = new User({
		firstName : reqBody.firstName,
		lastName : reqBody.lastName,
		email : reqBody.email,
		mobileNo : reqBody.mobileNo,
		password : bcrypt.hashSync(reqBody.password, 10)
	})

	return newUser.save().then((user, error) => {
		if(error){
			return false;
		}
		else{
			return true;
		}
	})
}


// User authentication
module.exports.loginUser = (reqBody) => {

	return User.findOne({email: reqBody.email}).then(result => {
		if(result == null){
			return false;
		}
		else{
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password);

			if(isPasswordCorrect){
				return { access: auth.createAccessToken(result) }
			}
			else{
				return false;
			}
		}
	})
}

// Activity s33
// module.exports.getProfile = (data) => {
// 	return User.findById(data.userId).then(result => {
// 		// if(result == null) {
// 		// 	return false;
// 		// } else {
// 			// result.password = "";
// 			return result;
		
// 	})
// }

module.exports.getUsers = () => {
	return User.find({}).then(result => {
		if(result == null) {
			return false;
		} else {
			return result;
		}
	})
}

// Enroll user to a class
// Async await will be used in enrolling the user because we will need to update two separate documents when enrolling a user
module.exports.enroll = async (data) => {
	
	// Add the course ID in the enrollments array of the user
	let isUserUpdated = await User.findById(data.userId).then(user => {
		// Adds the courseId in the user's enrollment array
		user.enrollments.push({courseId : data.courseId});

		// Saves the updated user information in the database
		return user.save().then((result, error) => {
			if(error){
				return false;
			}
			else{
				return true;
			}
		})
	})


	// Add the user ID in the enrollees array of the course
		let isCourseUpdated = await Course.findById(data.courseId).then(course => {
			// Adds the userId in the course's enrollees array
			course.enrollees.push({userId : data.userId});

			// Save the updated course information in the database
			return course.save().then((course, error) => {
				if(error){
					return false;
				}
				else{
					return true;
				}
			})
		})

		// Condition that will check if the user and course documents have been updated
		if(isUserUpdated && isCourseUpdated){
			return true;
		}
		else{
			return false;
		}
	}

