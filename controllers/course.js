const Course = require("../model/Course");
const bcrypt = require("bcrypt");
const auth = require("../auth");


// Create a new course
module.exports.addCourse = async (user, reqBody) => {
	if(user.isAdmin){
		let newCourse = new Course ({
			name: reqBody.name,
			description: reqBody.description,
			price: reqBody.price
		})

		newCourse.save().then((course,error) => {
			if(error){
				return false;
			}
			else{
				return true;
			}
		})
	}
	else{
		return("You have no access.")
	}
}

// Controlller method for retrieveing  all the courses
module.exports.getAllCourses = () => {
	return Course.find({}).then(result => {
		return result;
	})
}

// retrieve all active courses
module.exports.getAllActive = () => {
	return Course.find({isActive : true}).then(result => {
		return result;
	})
}

// Retrieving a specific course
module.exports.getCourse = (reqParams) => {
	return Course.findById(reqParams.courseId).then(result => {
		return result;
	})
}

// Update Course
module.exports.updateCourse = async (user, reqParams, reqBody) => {
	if(user.isAdmin){
		let updatedCourse = {
			name : reqBody.name,
			description : reqBody.description,
			price: reqBody.price
		}

		return Course.findByIdAndUpdate(reqParams.courseId, updatedCourse).then((course, error) => {
			if(error){
				return false;
			}
			else{
				return true;
			}
		})
		
		}else{
			return (`You have no access`);
	}
	// Specify the fields/properties of the documents to be updated
	
}

// Archive Course
module.exports.archiveCourse = async (reqParams, user) => {
	if(user.isAdmin){
		return Course.findByIdAndUpdate(reqParams, { $set: { isActive: false }}).then((course, error) => {
			if(error){
				return false;
			}
			else{
				return true;
			}
		})
		
		}else{
			return (`You have no access`);
	}
	
}
